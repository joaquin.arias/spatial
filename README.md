# CLP Spatial reasoner #

The `CLP Spatial` reasoner is a top-down interpreter for BIM models
using constraints.

This work was developed in Helsinki (Summer'19) in collaboration with
Dr. Seppo Törmän, CEO of ([VisuaLynk](https://visualynk.com/#/)).


## Introduction

`CLP Spatial` by [Joaquin Arias](mailto:joaquin.arias@imdea.org), is
an implementation of a Spatial reasoner based on Constraint Logic
Programming using CLP(R), the lineal equation solver over reals by
Christian Holzbauer. Unlike similar systems, it does allows the same
formalism to express queries regarding ontology, time, changes,
progress, among others. This allows `CLP Spatial` complex
reasoning model that can be used to infer back-forward results.

## Installation of Ciao Prolog and CLP Spatial

### For Mac-OS / Linux

To install CLP Spatial you need
[`Ciao Prolog`](https://github.com/ciao-lang/ciao) running in your
computer.

First, install `Ciao Prolog` using:

```
curl https://ciao-lang.org/boot -sSfL | sh
```

Then, get and install CLP Spatial using:

```
ciao get gitlab.software.imdea.org/joaquin.arias/spatial
```

## Examples for CLP Spatial usage:

Go to the folder with the examples (expected location: `~/.ciao/spatial/examples/`)

First, open a shell of ciao.

```
$ ciao
```

Then, load an example (e.g., example_01_duplex.pl).

```
?- [example_01_duplex].
yes
```

And run the queries.

```
?- query01.
yes

?- query02(X).
X = 'IfcBeam' ? ;
X = 'IfcCovering' ? ;
X = 'IfcDoor' ? ;
X = 'IfcFooting' ? 
...
```

For each query/answer the CLP Spatial reasoner generates an output to
visualize the results in the folder where you are running the ciao
shell.

The output is a x3d file named `output.x3d` which can be visualized
opening the file `index.html` (located in the folder with the examples)
using a web browser.

To visualize the x3d file you may need to run a local web server
opening a new terminal prompt  (from the
folder with the examples) and using:

```
$ python -m SimpleHTTPServer 8000
```

or using:

```
$ php -S localhost:8000
```

and from a browser visiting the url `http://localhost:8000`

Note: the expected folder with the examples is
`~/.ciao/spatial/examples/` but you can download/move it to another
location.
