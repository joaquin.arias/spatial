:- module(_,_,[]).

:- use_package(spatial).
:- use_package(clpr).

:- use_module(library(hiordlib)).
:- use_module(library(aggregates)).
:- use_module(library(lists)).
:- use_module(library(streams)).

:- multifile object/5.
:- ensure_loaded('01_duplex_ARQ').

:- multifile set_point/2.
set_point(center, point(1.92,4.15,6.19)).
set_point(position, point(0,5,50)).  %% (_ z y)

%% Query Q1 -- ICLP'22
%% Output elements of generic type not doors/slab in blue and doors in green
q1 :-
    findall( Sh,
    (
        object(Type, _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
        Type \= 'IfcDoor',
        Type \= 'IfcSlab',
        box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh)
    ),
    Building),
    findall( Sh,
    (
        object('IfcDoor', _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
        box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh)
    ),
    Doors),
    output(
    [
        par(Doors,green),
        par(Building,blue)
    ]).

%% Query Q2 -- ICLP'22
q2 :-
    findall(SSh,
    (
        object(Type, _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
        Type \= 'IfcSlab',
        Type \= 'IfcDoor',
        box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh),
        AuxSh = shape([_,Y,_]),
        Y  .>=. -7,
        Y  .<. -4,
        shape_intersect([Sh], [AuxSh], SSh)
        
    ),
    Slice),
    flat(Slice,SliceFlat),
    findall(Sh,
    (
        Ya .<. -4,
        object('IfcDoor', _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
        box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh)
    ),
    Doors),
    output(
    [
        par(SliceFlat,blue),
        par(Doors,green)
    ]).



%%% Other QUERIES  %%%

%% Output elements of a type one by one
query02(Type) :-
    setof(T, [A,B,C,D]^object(T,A,B,C,D), Types),
    member(Type,Types),
    findall( Sh, (
                             object(Type, _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
                             box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh)
    ), IfcBeamSh),
    output(
    [
        par(IfcBeamSh,blue)
    ]).

%% Output each element (green) that intersect with others (blue)
query03(Type0-Id0) :-
    object_shape(Type0,Id0,Sh0),
    findall( Sh,
    (
        object_shape(_,Id,Sh),
        Id \= Id0,
        shape_intersect(Sh0, Sh, _)
    ),
    ListIdIntersect),
    output(
    [
        par([Sh0],red),
        par(ListIdIntersect,blue)
     ]).

query04 :-
    findall( Sh,
    (
        object(Type, _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
        Type \= 'IfcDoor',
        Type \= 'IfcSlab',
        box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh)
    ),
    IfcBeamSh),
    findall( Sh,
    (
        object('IfcDoor', _Id, (Xa,Ya,Za), (Xb,Yb,Zb), _),
        box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh)
    ),
    IfcDoorSh),
    output(
    [
        par(IfcDoorSh,green),
        par(IfcBeamSh,blue)
    ]).



flat([A],A).
flat([A|Rs],F) :-
    flat(Rs,Fs),
    append(A,Fs,F).
         

object_shape(Type, Id, Sh) :-
    object(Type,Id,(Xa,Ya,Za), (Xb,Yb,Zb), _),
    box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh).
    



%% time(X,Time) :-
%%     statistics(runtime, _),
%%     call(X),
%%     statistics(runtime, [_|[Time]]).

    