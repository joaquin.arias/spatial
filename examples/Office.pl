:- module(_,_).

:- use_package(spatial).

:- use_module(library(hiordlib)).

:- multifile object/5.
:- ensure_loaded('02_Trapelo_ARQ').
:- ensure_loaded('02_Trapelo_STR').

:- multifile set_point/2.
set_point(center, point(219913.789395994, 907202.6557156643, 45.380805356249894)).
set_point(position, point(0,2,100)).  %% (_ z y)


%% Query Q1 -- ICLP'22
%% Find first uncovered object:
%% - draw it in blue
%% - draw intersected objects in green
q1 :-
    Type0 = 'IfcBeam',
    object_shape(Type0,_Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        % Type='IfcOpeningElement',
        object_shape(_Type,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    IfcInt),
    %% Compute the uncovered area of Sh0
    shape_substract([Sh0], IfcInt, Uncovered),
    %% Discard if is not uncovered
    Uncovered \= [],
    output(
    [
        par([Sh0], blue),
        par(IfcInt, green)
    ]).

%% Query Q2 -- ICLP'22
%% Find first uncovered object (draw uncovered parts in red)
q2 :-
    Type0 = 'IfcBeam',
    object_shape(Type0,_Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        % Type='IfcOpeningElement',
        object_shape(_Type,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    IfcInt),
    %% Compute the uncovered area of Sh0
    shape_substract([Sh0], IfcInt, Uncovered),
    %% Discard if is not uncovered
    Uncovered \= [],
    output(
    [
         par(Uncovered, red)
    ]).


%% Performance evaluation -- ICLP'22
test :-
    q3(_-_-Uncovered),
    (  Uncovered == [],
       display('|')
    ;
        display('-')
    ), fail.
test.

%% Return elements intersected (green) and it uncovered area (red)
q3(Type0-Id0-Uncovered) :-
    Type0 = 'IfcBeam',
    object_shape(Type0,Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        % Type='IfcOpeningElement',
        object_shape(_Type,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    IfcInt),
    %% Compute the uncovered area of Sh0
    shape_substract([Sh0], IfcInt, Uncovered),
    % Uncovered \= [],

    output(
    [
        par([Sh0],blue)
        par(IfcInt,green)
        par(Uncovered, red)
    ]).




%%% Other QUERIES  %%%

%% Output elements for ARQ of type not doors/windows/slabs in blue
%% and doors/windows in green
query_01 :-
    %% Find all the shapes that are not IfcSlab, IfcDoor or IfcWindow
    findall( Sh,
    (
        object_shape(Type, _Id, Sh, arq),
        Type \= 'IfcSlab',
        Type \= 'IfcDoor',
        Type \= 'IfcWindow'
    ),
    IfcShs),
    %% Find all the shapes of type IfcDoor or IfcWindow
    findall( Sh,
    (
        object_shape('IfcDoor', _Id, Sh, arq)
    ;
        object_shape('IfcWindow', _Id, Sh, arq)
    ),
    IfcDoorWindowShs),
    %% Output the list of shapes in green or blue
    output(
    [
        par(IfcDoorWindowShs,green),
        par(IfcShs,blue)
    ]).

%% Output elements of a given Type in a given Model one by one
query_02(Type-Model) :-
    %% Find all the different types of object in the database
    setof(T,
    (
        [A,B,C,M]^object(T,A,B,C,M)
    ),
    Types),
    %% Pick one Type and one Model
    member(Type,Types),
    member(Model,[arq,str]),
    %% Find all the shapes of a given Type and Model
    findall( Sh,
    (
        object_shape(Type, _Id, Sh, Model)
    ),
    IfcSh),
    %% Output
    output(
    [
        par(IfcSh,blue)
    ]).

%% Output each element (red) in ARQ that intersect with others (blue) in STR
query_03(Type0-Id0) :-
    object_shape(Type0,Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        object_shape(_,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    ListShIntersect),

    output(
    [
        par([Sh0],red),
        par(ListShIntersect,blue)
    ]).

%% Return elements intersected (green) and it uncovered area (red)
query_04(Type0-Id0-Uncovered) :-
    Type0 = 'IfcBeam',
    object_shape(Type0,Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        % Type='IfcOpeningElement',
        object_shape(_Type,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    IfcInt),
    %% Compute the uncovered area of Sh0
    shape_substract([Sh0], IfcInt, Uncovered),
    % Uncovered \= [],

    output(
    [
%%        par([Sh0],blue)
        %% par(IfcInt,green)
         par(Uncovered, red)
    ]).

%% Find first uncovered object (draw object parts in orange)
query_04a :-
    Type0 = 'IfcBeam',
    object_shape(Type0,_Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        % Type='IfcOpeningElement',
        object_shape(_Type,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    IfcInt),
    %% Compute the uncovered area of Sh0
    shape_substract([Sh0], IfcInt, Uncovered),
    %% Discard if is not uncovered
    Uncovered \= [],
    output(
    [
        par([Sh0], blue),
        par(IfcInt, green)
    ]).

%% Find first uncovered object (draw uncovered parts in red)
query_04b :-
    Type0 = 'IfcBeam',
    object_shape(Type0,_Id0,Sh0,arq),
    %% Find all shapes whose interesection with Sh0 is not empty
    findall( Sh,
    (
        % Type='IfcOpeningElement',
        object_shape(_Type,_Id,Sh,str),
        shape_intersect([Sh0], [Sh], Int),
        Int \= []
    ),
    IfcInt),
    %% Compute the uncovered area of Sh0
    shape_substract([Sh0], IfcInt, Uncovered),
    %% Discard if is not uncovered
    Uncovered \= [],
    output(
    [
         par(Uncovered, red)
    ]).

%% Output specific shapes
query_05 :-
    object_shape('IfcOpeningElement','3R8Akq70r31eG8KzMbLqc0',Sh1,arq),
    object_shape('IfcOpeningElement','0VLqpJ9br8dO7kBcL8md9m',Sh2,arq),
    object_shape('IfcOpeningElement','1v21fhzrj7cu3W$TsLj6Xe',Sh3,arq),

    output(
    [
         par([Sh1],green),
         par([Sh2],red),
         par([Sh3],blue)
    ]).
    
%% Generate the object shape from the object/5 database
object_shape(Type, Id, Sh, Model) :-
    object(Type,Id,(Xa,Ya,Za), (Xb,Yb,Zb), Model),
    box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh).

time(Query, Time) :-
    statistics(runtime,_),
    call(Query),
    statistics(runtime,[_,Time]).
    

%% Find all incovered object query04.
test_time(T) :-
    statistics(runtime,_),
    test,
    statistics(runtime,[_,T]).

test :-
    query_04(_-_-Uncovered),
    (
        Uncovered == [],
        display('|')
    ;
        display('-')
    ),
    fail.
test.
