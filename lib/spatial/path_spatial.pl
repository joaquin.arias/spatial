:- module(_,_).


:- use_package(clpr).
:- use_module(spatial_clpr).


path(point(Xa,Ya,Za,Ta), point(Xb,Yb,Zb,Tb), [X,Y,Z,T]) :-
    apply_clp_constraints([ X .=. Xa + (Xb-Xa)*K, Y .=. Ya + (Yb-Ya)*K, Z .=. Za + (Zb-Za)*K, T .=. Ta + (Tb-Ta)*K, K .>=. 0, K .<. 1 ]).


line(point(Xa,Ya,Za,Ta), point(Xb,Yb,Zb,Tb), [LX,LY,LZ]) :-
    path(point(Xa,Ya,Za,Ta), point(Xb,Yb,Zb,Tb), [X,Y,Z,_T]),
    dump_clp_constraints([X,Y,Z],[LX,LY,LZ],Line),
    apply_clp_constraints(Line).


    
