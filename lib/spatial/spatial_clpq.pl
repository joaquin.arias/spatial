:- module(spatial_clpq,_,[]).


:- use_package(clpq).
:- use_module(library(clpq/clpq_dump), [
    clpqr_dump_constraints/3
                                   ]).
:- use_module(library(aggregates)).

apply_clp_constraints(Constraints) :-
    clpq_meta(Constraints).
dump_clp_constraints(Var,NewVar,NewConst) :-
    clpqr_dump_constraints(Var,NewVar,NewConst).
    
duals_clp(a(Vars,Constraints), Duals) :-
    findall(a(Vars,Dual), dual_clp(Constraints, Dual), Duals).



dual_clp([Unique], [Dual]) :-
    dual_clp_(Unique, Dual).
dual_clp([Init, Next|Is], Dual) :-
    (
        dual_clp([Init], Dual)
    ;
        dual_clp([Next|Is], NextDual),
        Dual = [Init| NextDual]
    ).

dual_clp_(A .<. B, A .>=. B).
dual_clp_(A .=<. B, A .>. B).
dual_clp_(A .>. B, A .=<. B).
dual_clp_(A .>=. B, A .<. B).
dual_clp_(A .<>. B, A .=. B).
dual_clp_(A .=. B, A .>. B).
dual_clp_(A .=. B, A .<. B).    


rat_num([],[]).
rat_num([V|Vs],[N|Ns]) :-
    (
        V = rat(A,B) ->
        N is A/B
    ;
        N = V
    ),
    rat_num(Vs,Ns).
