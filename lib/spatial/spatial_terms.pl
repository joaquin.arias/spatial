:- module(_,_,[]).


%:-use_package(clpq).
%:- use_module(spatial_clpq, [
:-use_package(clpr).
:- use_module(library(spatial/spatial_clpr), [
    apply_clp_constraints/1,
    dump_clp_constraints/3,
    dual_clp/2
                   ]).
:- use_module(library(lists)).
:- use_module(library(aggregates)).
:- use_module(library(format)).
:- use_module(library(streams)).



% shape(Vars, Constraints).
% empty = []
% all = [shape(_,[])]


% shape_union(Sh1, Sh2, ShUnion).
% shape_intersection(Sh1, Sh2, ShIntersection).
% shape_intersection_diff(Sh1, Sh2, ShIntersection, Sh1NotIntersected).

%% shape_complementary(Sh1, ShComplementary).

% shape_intersect(Sh1, Sh2).
% shape_disjoint(Sh1, Sh2).


rec_shape(point(Xa,Ya), point(Xb,Yb), Sh) :-
    Sh = shape([X,Y], [X .>=. Xa, X .<. Xb,Y .>=. Ya, Y .<. Yb]).
box_shape(point(Xa,Ya,Za), point(Xb,Yb,Zb), Sh) :-
    Sh = shape([X,Y,Z], [X .>=. Xa, X .<. Xb, Y .>=. Ya, Y .<. Yb, Z .>=. Za, Z .<. Zb]).

convex([point(Xa,Ya), point(Xb,Yb)], shape([X,Y], [(Xb-Xa) * (Y-Ya) - (X-Xa) * (Yb - Ya).=<.0])).
convex([A,B,C|Cs],shape(V,[G|Gs])) :- convex([A,B],shape(V,[G])), convex([B,C|Cs],shape(V,Gs)).



shape_union(Sh1s, Sh2s, ShUnions) :-
    append(Sh1s, Sh2s, ShUnions).


shape_intersect(Sh1s, Sh2s, ShIntersects) :-
    shape_intersect_(Sh1s, Sh2s, [], ShIntersects).

shape_intersect_([],_,ShInt,ShInt) :- !.
shape_intersect_(_,[],ShInt,ShInt) :- !.
shape_intersect_([Sh1|Sh1s],[Sh2|Sh2s],ShInt0,ShInt) :-
    intersect_shape_([Sh1], [Sh2], Sh12),
    shape_union(ShInt0,Sh12,ShInt1),
    shape_intersect_([Sh1], Sh2s, ShInt1,ShInt2),
    shape_intersect_(Sh1s,[Sh2|Sh2s],ShInt2,ShInt).

intersect_shape_([shape(Vars,Geo1)],[shape(Vars,Geo2)],ShInt) :-
    copy_term([Vars,Geo1,Geo2],[CopyVars,CopyGeo1, CopyGeo2]),
    (
        apply_clp_constraints(CopyGeo1),
        apply_clp_constraints(CopyGeo2), true ->
        dump_clp_constraints(CopyVars, Vars, GeoInt),
        ShInt = [shape(Vars,GeoInt)]
    ;
        ShInt = []
    ).

shape_substract(Sh1s, Sh2s, Sh1Remains) :-
    shape_substract_(Sh1s, Sh2s, Sh1Remains),!.

shape_substract_([],_,[]) :- !.
shape_substract_(Sh1s,[],Sh1s) :- !.
shape_substract_([Sh1|Sh1s],Sh2s, Sh1Remains) :-
    substract_shape([Sh1],Sh2s,Remain0),
    shape_substract_(Sh1s,Sh2s,Remain1),
    shape_union(Remain0,Remain1,Sh1Remains).
    
substract_shape(Sh1,[Sh2|Sh2s],Sh1Remains) :-
    (
        intersect_shape_(Sh1,[Sh2],ShInt),
        ShInt == [] ->
        shape_substract_(Sh1,Sh2s,Sh1Remains)
    ;
        shape_complement([Sh2], NotSh2),
        %   display(fin(Geo1,DualGeo2)),nl,
        shape_intersect(Sh1, NotSh2, ShRemain0),
        shape_substract_(ShRemain0,Sh2s,Sh1Remains)
    ).


shape_complement([], [shape(_,[])]) :- !.
shape_complement([shape(_,[])], []) :- !.
shape_complement([shape(Vars,Geo2)], NotSh2s) :-
    setof(shape(Vars,Dual), Geo2^dual_clp(Geo2, Dual), NotSh2s).



% shape_intersection_diff(Sh1, Sh2, ShIntersection, Sh1NotIntersected) :-
%       cover_shapes(Sh1, Sh2, ShIntersection, Sh1NotIntersected).

% shape_intersect(Sh1, Sh2) :-
%       shape_intersection_diff(Sh1, Sh2, ShIntersect, _),
%       not_empty(ShIntersect).
    
% cover_shapes(shape(Vars,[]),_,shape(Vars,[]),shape(Vars,[])) :- true, !.
% cover_shapes(shape(Vars,Geo1s),shape(Vars,[]),shape(Vars,[]),shape(Vars,Geo1s)).
% cover_shapes(shape(Vars,[Geo1|Geo1s]),shape(Vars,[Geo2|Geo2s]),shape(Vars,Intersect0s),shape(Vars,Remain0s)) :-
%           cover_shape(Vars, Geo1, Geo2, Intersect1, Remain1s), 
%           cover_shapes(shape(Vars,Remain1s), shape(Vars,Geo2s), shape(Vars,Intersect2s), shape(Vars,Remain2s)),
%           cover_shapes(shape(Vars,Geo1s), shape(Vars,[Geo2|Geo2s]), shape(Vars,Intersect3s), shape(Vars,Remain3s)),
%           append([Intersect1|Intersect2s], Intersect3s, Intersect0s),
%           append(Remain2s, Remain3s, Remain0s).

% cover_shape(Vars, Geo1, Geo2, Intersect1, Remain1s) :-
%       intersect_shape(Vars, Geo1, Geo2, Intersect1),
%       (
%           Intersect1 == [], true ->
%           Remain1s = [Geo1]
%       ;
%           substract_shape(Vars, Geo1, Geo2, Remain1s)
%       ).
    
% intersect_shape(Vars, Geo1, Geo2, Intersect1) :-
%       copy_term([Vars,Geo1,Geo2],[CopyVars,CopyGeo1, CopyGeo2]),
%       (
%           apply_clp_constraints(CopyGeo1),
%           apply_clp_constraints(CopyGeo2), true ->
%           dump_clp_constraints(CopyVars, Vars, Intersect1)
%       ;
%           Intersect1 = []
%       ).

% substract_shape(Vars, Geo1, Geo2, Remain1s) :-
%       findall(Dual, dual_clp(Geo2, Dual), DualGeo2),
% %     display(fin(Geo1,DualGeo2)),nl,
%       intersect_shapes(Vars, Geo1, DualGeo2, Remain1s).

% intersect_shapes(_Vars, _, [], []).
% intersect_shapes(Vars, Geo1, [Dual|DualGeo2], Remain0s) :-
%       intersect_shapes(Vars, Geo1, DualGeo2, Remain1s),
%       intersect_shape(Vars, Geo1, Dual, Remain2),
%       (
%           Remain2 == [], true ->
%           Remain0s = Remain1s
%       ;
%           Remain0s = [Remain2 | Remain1s]
%       ).


%%%%%% OUTPUT


color_pallete(green, rgb(0,1,0)).
color_pallete(red, rgb(1,0,0)).
color_pallete(blue, rgb(0,0,1)).

output(List) :-
    File = 'output.x3d',
    open_3d_file(File,Stream),
    print_3d_List(List,Stream),
    close_3d_file(File,Stream).

print_3d_List([],_).
print_3d_List([par(Shs,Color)|List],Stream) :-
    \+ \+ print_3d(Shs,Color,Stream),
    print_3d_List(List,Stream).

:- multifile set_point/2.
take_point(Type,Center) :- if(set_point(Type,Center),true,Center=point(0,0,0)).

print_3d([],_,_) :- !.
print_3d([shape(Vars, A)|Shs],Color,Stream) :-
    copy_term([Vars,A],[[X,Y,Z],CopyA]),
    apply_clp_constraints(CopyA), !, %% it could fail
    dump_clp_constraints([X],[Nx],Ax),
    dump_clp_constraints([Y],[Ny],Ay),
    dump_clp_constraints([Z],[Nz],Az),
    member('.>=.'(Nx,Infx),Ax),
    member('.<.'(Nx,Supx),Ax),
    member('.>=.'(Ny,Infy),Ay),
    member('.<.'(Ny,Supy),Ay),
    member('.>=.'(Nz,Infz),Az),
    member('.<.'(Nz,Supz),Az),
    take_point(center,point(CenterX, CenterY, CenterZ)),
    SizeX is Supx - Infx,   SizeY is Supy - Infy,   SizeZ is Supz - Infz,
    CX is Infx + SizeX/2 - CenterX, CY is -1 * (Infy + SizeY/2 - CenterY), CZ is Infz + SizeZ/2 - CenterZ,
    color_pallete(Color,rgb(R,G,B)),
    %% ?? (X Z Y)
    format(Stream,'    <Transform translation="~2f ~2f ~2f"> \n       <Shape> \n    <Appearance> \n           <Material diffuseColor="~p ~p ~p" specularColor="0 1 0" transparency=".4"/> \n        </Appearance>  \n         <Box size="~2f ~2f ~2f"/> \n       </Shape> \n     </Transform> \n  \n ',[CX, CZ, CY, R,G,B, SizeX, SizeZ, SizeY]),
    print_3d(Shs,Color,Stream).

print_3d([_|Shs],Color,Stream) :-     %% for inconsistent areas
    print_3d(Shs,Color,Stream).

    

            
open_3d_file(File,Stream) :-
    open_my_file(File,Stream),
    print_3d_init(Stream).
close_3d_file(File,Stream) :-
    print_3d_end(Stream),
    close_my_file(File,Stream).


open_my_file(File,Stream) :-
    open(File,append,_F),close(_F),
    open(File,write,Stream).

close_my_file(_File,Stream) :-
    close(Stream).


print_3d_init(Stream) :-
    take_point(position,point(Posx, Posy, Posz)),
    format(Stream, '<?xml version="1.0" encoding="UTF-8"?> \n <!DOCTYPE X3D PUBLIC "ISO//Web3D//DTD X3D 3.3//EN" "http://www.web3d.org/specifications/x3d-3.3.dtd"> \n <X3D profile="Interchange" version="3.3" xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance" xsd:noNamespaceSchemaLocation="http://www.web3d.org/specifications/x3d-3.3.xsd"> \n  \n   <Scene> \n     <Background skyColor="1 1 1"/>\n\n     <Viewpoint position="~p ~p ~p"></Viewpoint>\n\n',[Posx,Posy,Posz]).

print_3d_end(Stream) :-
    format(Stream, '  </Scene> \n  \n </X3D>',[]).


