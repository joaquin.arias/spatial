:- module(spatial_clpr,_,[]).


:- use_package(clpr).
:- use_module(library(clpr/clpr_dump), [
    clpqr_dump_constraints/3
                                   ]).
:- use_module(library(aggregates)).

apply_clp_constraints(Constraints) :-
    clpr_meta(Constraints).
dump_clp_constraints(Var,NewVar,NewConst) :-
    clpqr_dump_constraints(Var,NewVar,NewConst).
    
duals_clp(a(Vars,Constraints), Duals) :-
    findall(a(Vars,Dual), dual_clp(Constraints, Dual), Duals).



dual_clp([Unique], [Dual]) :-
    dual_clp_(Unique, Dual).
dual_clp([Init, Next|Is], Dual) :-
    (
        dual_clp([Init], Dual)
    ;
        dual_clp([Next|Is], NextDual),
        Dual = [Init| NextDual]
    ).

dual_clp_(A .<. B, A .>=. B).
dual_clp_(A .=<. B, A .>. B).
dual_clp_(A .>. B, A .=<. B).
dual_clp_(A .>=. B, A .<. B).
dual_clp_(A .<>. B, A .=. B).
dual_clp_(A .=. B, A .>. B).
dual_clp_(A .=. B, A .<. B).    

rat_num(X,X).

